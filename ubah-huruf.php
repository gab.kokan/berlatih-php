<?php
function ubah_huruf($string){
    $huruf = "abcdefghijklmnopqrstuvwxyz";
    $hasil = "<br>Ubah Huruf <b> $string </b> adalah ";
    for ($i=0; $i < strlen($string) ; $i++) {
      $posisi = strrpos($huruf, $string[$i]);
      $hasil .= substr($huruf, $posisi + 1, 1);
    }
    return $hasil;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>
