<?php
  function tentukan_nilai($number){
    $hasil = "";
    if ($number >= 85 && $number < 100){
      $hasil .= "Nilai $number Sangat Baik <br>";
    }else if($number >= 70 && $number < 85){
      $hasil .= "Nilai $number Baik <br>";
    }else if ($number >= 60 && $number < 70){
      $hasil .= "Nilai $number Cukup <br>";
    }else{
      $hasil .= "Nilai $number Kurang <br>";
    }
    return $hasil;
  }
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>
